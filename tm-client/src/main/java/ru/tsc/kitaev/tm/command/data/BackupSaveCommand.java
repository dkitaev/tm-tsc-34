package ru.tsc.kitaev.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.command.AbstractCommand;
import ru.tsc.kitaev.tm.endpoint.Session;

public class BackupSaveCommand extends AbstractCommand {

    @NotNull
    public final static String BACKUP_SAVE = "backup-save";

    @NotNull
    @Override
    public String name() {
        return BACKUP_SAVE;
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Save backup data.";
    }

    @Override
    public void execute() {
        @Nullable final Session session = serviceLocator.getSessionService().getSession();
        serviceLocator.getAdminEndpoint().dataBackupSave(session);
    }

}
