package ru.tsc.kitaev.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.endpoint.Session;

public interface ISessionService {

    @Nullable
    Session getSession();

    void setSession(@Nullable final Session session);

}
