package ru.tsc.kitaev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.command.AbstractProjectCommand;
import ru.tsc.kitaev.tm.endpoint.Session;
import ru.tsc.kitaev.tm.enumerated.Sort;
import ru.tsc.kitaev.tm.endpoint.Project;
import ru.tsc.kitaev.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class ProjectListShowCommand extends AbstractProjectCommand {

    @NotNull
    @Override
    public String name() {
        return "project-list";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show project list...";
    }

    @Override
    public void execute() {
        @Nullable final Session session = serviceLocator.getSessionService().getSession();
        System.out.println("Enter sort");
        System.out.println(Arrays.toString(Sort.values()));
        @Nullable final String sort = TerminalUtil.nextLine();
        @Nullable final List<Project> projects;
        System.out.println("[SHOW PROJECTS]");
        if (sort.isEmpty()) projects = serviceLocator.getProjectEndpoint().findProjectAll(session);
        else {
            @NotNull Sort sortType = Sort.valueOf(sort);
            System.out.println(sortType.getDisplayName());
            projects = serviceLocator.getProjectEndpoint().findProjectAllSorted(session, sort);
        }
        for (@NotNull final Project project: projects) {
            System.out.println(projects.indexOf(project) + 1 + ". " +project.toString() + ". " + project.getStatus());
        }
        System.out.println("[OK]");
    }

}
