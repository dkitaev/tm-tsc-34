package ru.tsc.kitaev.tm.exception.empty;

import org.jetbrains.annotations.NotNull;
import ru.tsc.kitaev.tm.exception.AbstractException;

public final class EmptyRoleException extends AbstractException {

    public EmptyRoleException() {
        super("Error. Role is empty.");
    }

    public EmptyRoleException(@NotNull String value) {
        super("Error" + value + " Role is empty.");
    }

}
