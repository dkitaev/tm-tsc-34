package ru.tsc.kitaev.tm.component;

import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.api.repository.IProjectRepository;
import ru.tsc.kitaev.tm.api.repository.ISessionRepository;
import ru.tsc.kitaev.tm.api.repository.ITaskRepository;
import ru.tsc.kitaev.tm.api.repository.IUserRepository;
import ru.tsc.kitaev.tm.api.service.*;
import ru.tsc.kitaev.tm.endpoint.*;
import ru.tsc.kitaev.tm.enumerated.Role;
import ru.tsc.kitaev.tm.repository.ProjectRepository;
import ru.tsc.kitaev.tm.repository.SessionRepository;
import ru.tsc.kitaev.tm.repository.TaskRepository;
import ru.tsc.kitaev.tm.repository.UserRepository;
import ru.tsc.kitaev.tm.service.*;
import ru.tsc.kitaev.tm.enumerated.Status;
import ru.tsc.kitaev.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

@Getter
@Setter
public final class Bootstrap implements IServiceLocator {

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(taskRepository, projectRepository);

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final ISessionRepository sessionRepository = new SessionRepository();

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IUserService userService = new UserService(userRepository, propertyService);

    @NotNull
    private final IAuthService authService = new AuthService(userService, propertyService);

    @NotNull
    private final ILogService logService = new LogService();

    @NotNull
    private final IDomainService domainService = new DomainService(this);

    @NotNull
    private final ISessionService sessionService = new SessionService(userService, sessionRepository, propertyService);

    @NotNull
    private final Backup backup = new Backup(domainService);

    @NotNull
    private final ProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final TaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    private final ProjectTaskEndpoint projectTaskEndpoint = new ProjectTaskEndpoint(this);

    @NotNull
    private final UserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    private final AdminEndpoint adminEndpoint = new AdminEndpoint(this);

    @NotNull
    private final AdminUserEndpoint adminUserEndpoint = new AdminUserEndpoint(this);

    @NotNull
    private final SessionEndpoint sessionEndpoint = new SessionEndpoint(this);

    public void start(@Nullable final String[] args) {
        try {
            initPID();
            initData();
            initEndpoint();
            backup.init();
            } catch (@NotNull final Exception e) {
                logService.error(e);
                System.exit(1);
            }
    }

    private void initEndpoint() {
        registry(projectEndpoint);
        registry(taskEndpoint);
        registry(projectTaskEndpoint);
        registry(userEndpoint);
        registry(adminEndpoint);
        registry(adminUserEndpoint);
        registry(sessionEndpoint);
    }

    private void initData() {
        String userId = userService.create("test", "test", "test@email.ru").getId();
        String adminId = userService.create("admin", "admin", Role.ADMIN).getId();
        projectService.create(userId, "Project C", "-").setStatus(Status.COMPLETED);
        projectService.create(userId, "Project A", "-");
        projectService.create(adminId, "Project B", "-").setStatus(Status.IN_PROGRESS);
        projectService.create(adminId, "Project D", "-").setStatus(Status.NOT_STARTED);
        taskService.create(userId, "Task C", "-").setStatus(Status.COMPLETED);
        taskService.create(userId, "Task A", "-");
        taskService.create(adminId, "Task B", "-").setStatus(Status.IN_PROGRESS);
        taskService.create(adminId, "Task D", "-").setStatus(Status.NOT_STARTED);
    }

    private void registry(@Nullable final Object endpoint) {
        if (endpoint == null) return;
        final String host = propertyService.getServerHost();
        final Integer port = propertyService.getServerPort();
        final String name = endpoint.getClass().getSimpleName();
        final String wsdl = "http://" + host + ":" + port + "/" + name + "?wsdl";
        System.out.println(wsdl);
        Endpoint.publish(wsdl, endpoint);
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

}
