package ru.tsc.kitaev.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.api.endpoint.IAdminEndpoint;
import ru.tsc.kitaev.tm.api.service.IServiceLocator;
import ru.tsc.kitaev.tm.dto.Domain;
import ru.tsc.kitaev.tm.enumerated.Role;
import ru.tsc.kitaev.tm.exception.AbstractException;
import ru.tsc.kitaev.tm.model.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
@NoArgsConstructor
public class AdminEndpoint extends AbstractEndpoint implements IAdminEndpoint {

    public AdminEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @WebMethod
    public void dataBackupLoad(
            @WebParam(name = "session", partName = "session") @Nullable Session session
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDomainService().dataBackupLoad();
    }

    @Override
    @WebMethod
    public void dataBackupSave(
            @WebParam(name = "session", partName = "session") @Nullable Session session
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDomainService().dataBackupSave();
    }

    @Override
    @WebMethod
    public void dataBase64Load(
            @WebParam(name = "session", partName = "session") @Nullable Session session
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDomainService().dataBase64Load();
    }

    @Override
    @WebMethod
    public void dataBase64Save(
            @WebParam(name = "session", partName = "session") @Nullable Session session
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDomainService().dataBase64Save();
    }

    @Override
    @WebMethod
    public void dataBinLoad(
            @WebParam(name = "session", partName = "session") @Nullable Session session
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDomainService().dataBinLoad();
    }

    @Override
    @WebMethod
    public void dataBinSave(
            @WebParam(name = "session", partName = "session") @Nullable Session session
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDomainService().dataBinSave();
    }

    @Override
    @WebMethod
    public void dataJsonLoadFasterXML(
            @WebParam(name = "session", partName = "session") @Nullable Session session
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDomainService().dataJsonLoadFasterXML();
    }

    @Override
    @WebMethod
    public void dataJsonSaveFasterXML(
            @WebParam(name = "session", partName = "session") @Nullable Session session
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDomainService().dataJsonSaveFasterXML();
    }

    @Override
    @WebMethod
    public void dataJsonLoadJaxB(
            @WebParam(name = "session", partName = "session") @Nullable Session session
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDomainService().dataJsonLoadJaxB();
    }

    @Override
    @WebMethod
    public void dataJsonSaveJaxB(
            @WebParam(name = "session", partName = "session") @Nullable Session session
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDomainService().dataJsonSaveJaxB();
    }

    @Override
    @WebMethod
    public void dataXmlLoadFasterXML(
            @WebParam(name = "session", partName = "session") @Nullable Session session
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDomainService().dataXmlLoadFasterXML();
    }

    @Override
    @WebMethod
    public void dataXmlSaveFasterXML(
            @WebParam(name = "session", partName = "session") @Nullable Session session
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDomainService().dataXmlSaveFasterXML();
    }

    @Override
    @WebMethod
    public void dataXmlLoadJaxB(
            @WebParam(name = "session", partName = "session") @Nullable Session session
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDomainService().dataXmlLoadJaxB();
    }

    @Override
    @WebMethod
    public void dataXmlSaveJaxB(
            @WebParam(name = "session", partName = "session") @Nullable Session session
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDomainService().dtaXmlSaveJaxB();
    }

    @Override
    @WebMethod
    public void dataYamlLoadFasterXML(
            @WebParam(name = "session", partName = "session") @Nullable Session session
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDomainService().dataYamlLoadFasterXML();
    }

    @Override
    @WebMethod
    public void dataYamlSaveFasterXML(
            @WebParam(name = "session", partName = "session") @Nullable Session session
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDomainService().dataYamlSaveFasterXML();
    }

}
