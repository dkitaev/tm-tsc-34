package ru.tsc.kitaev.tm.exception.empty;

import org.jetbrains.annotations.NotNull;
import ru.tsc.kitaev.tm.exception.AbstractException;

public final class EmptyLoginException extends AbstractException {

    public EmptyLoginException() {
        super("Error. Login is empty.");
    }

    public EmptyLoginException(@NotNull String value) {
        super("Error" + value + " Login is empty.");
    }

}
